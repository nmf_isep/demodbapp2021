/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.ui.console;

import app.dal.ClientDB;
import app.dal.DatabaseConnection;
import app.domain.model.Address;
import app.domain.model.Client;
import app.domain.model.Context;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nunocastro (changed by nelson freire)
 */
public class Main {

    public static void main(String[] args) {
        
        //Application context (contains the connection factory)
        Context context = Context.getInstance();
        DatabaseConnection dbc = context.getConnectionFactory().getDatabaseConnection();
        Connection conn = dbc.getConnection();
        try {
            conn.setAutoCommit(false);
        
            //Clients DAL.
            ClientDB clientDB = new ClientDB();
            
            //Create a client with 2 addresses.
            Client c = new Client();
            c.setId(1);
            c.setName("abc");
            c.setVatNr("123456789");
            Address a;

            a = new Address();
            a.setId(1);
            a.setCity("Porto");
            a.setPostalCode("4000-123");
            a.setStreet("Rua de S. Tomé");
            c.addAddress(a);

            a = new Address();
            a.setId(2);
            a.setCity("Braga");
            a.setPostalCode("4200-456");
            a.setStreet("Praça da República, 556");
            c.addAddress(a);

            if (!clientDB.save(dbc, c)) {
                System.out.println("clientDB.save 1 dentro");
                throw dbc.getLastError();
            }
            conn.commit();
            
            //Change only the client's name.
            //update
            c.setName("abcd");
            if (!clientDB.save(dbc, c)) {
                throw dbc.getLastError();
            }
            conn.commit();
            
            //Change the 1st address.
            //update
            a = c.getAddressList().get(0);
            a.setStreet("Rua de S. Tomé, 231");
            if (!clientDB.save(dbc, c)) {
                throw dbc.getLastError();
            }
            conn.commit();
            
            //Delete the client and it's address associations (not the addresses).
            if (!clientDB.delete(dbc, c)) {
                throw dbc.getLastError();
            }
            conn.commit();
            
            //Try to recreate the client, but with an invalid address...
            //...to verify that none of the changes are persisted in the database.
            //Note: the maximum length of the street field in the database is 30...
            //...so we try to add a street with more than 30 characters.
            a = c.getAddressList().get(0);
            a.setStreet("Rua de S. Tomé, 231, Porto, Portugal"); //36 characters
            if (!clientDB.save(dbc, c)) {
                throw dbc.getLastError();
            }
            conn.commit();

            // Essencial. Caso contrário ligação bloqueia.
            // Verificar sessões abertas
            conn.close();
        }
        catch (SQLException ex) {
            Logger.getLogger(ClientDB.class.getName()).log(Level.SEVERE, null, ex);
            try {
                conn.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(ClientDB.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
}
