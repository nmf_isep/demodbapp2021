/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dal;

import app.domain.shared.Constants;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author nunocastro (changed by nelson freire)
 */
public class ConnectionFactory {

    //This is the size of the connection pool.
    private final Integer connectionPoolCount = 1;
    //
    private Integer connectionPoolRequest = 0;    
    private static ConnectionFactory cf;
    private final List<DatabaseConnection> dbConnectionList = new ArrayList<>();
    //
    private Properties props;

    public ConnectionFactory(){
        props = getProperties();
    }
    
    public static ConnectionFactory getConnectionFactory() {
        if (cf == null) {
            cf = new ConnectionFactory();
        }
        return cf;
    }

    public DatabaseConnection getDatabaseConnection() {
        DatabaseConnection c;
        if (++connectionPoolRequest > connectionPoolCount) {
            connectionPoolRequest = 1;
        }
        if (connectionPoolRequest > dbConnectionList.size()) {
            // ANC
            /*
            c = new DatabaseConnection(
                    "jdbc:oracle:thin:@vsrvbd1.dei.isep.ipp.pt:1521/pdborcl",                   
                    "MYUSER",
                    "mypassword");
             */
            c = new DatabaseConnection(
                    props.getProperty(Constants.PARAMS_DATABASE_URL),
                    props.getProperty(Constants.PARAMS_DATABASE_USERNAME),
                    props.getProperty(Constants.PARAMS_DATABASE_PASSWORD)
            );
            dbConnectionList.add(c);
        }
        else {
            c = dbConnectionList.get(connectionPoolRequest-1);
        }
        return c;
    }

    private Properties getProperties()
    {
        Properties props = new Properties();

        // Add default properties and values
        // ESOFT - props.setProperty(Constants.PARAMS_COMPANY_DESIGNATION, "Many Labs");

        // Read configured values
        try
        {
            InputStream in = new FileInputStream(Constants.PARAMS_FILENAME);
            props.load(in);
            in.close();
        }
        catch(IOException ex)
        {
            System.out.println(ex.getMessage());
        }
        return props;
    }
}
