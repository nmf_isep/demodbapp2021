/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dal;

import app.domain.model.Address;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nunocastro
 */
public class AddressDB implements IPersistable {

    @Override
    public boolean save(Object dbConnection, Object object) {
        DatabaseConnection dbc = (DatabaseConnection)dbConnection;
        Connection conn = dbc.getConnection();
        Address address = (Address)object;
        //
        String sqlCmd;
        sqlCmd = "select * from address where addressId = ?";
        try (PreparedStatement ps = conn.prepareStatement(sqlCmd)) {
            ps.setInt(1, address.getId());
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    sqlCmd = "update address set street = ?, postalcode = ?, city = ? where addressId = ?";
                }               
                else {
                    sqlCmd = "insert into address(street, postalcode, city, addressId) values (?, ?, ?, ?)";
                }
                //
                try (PreparedStatement ps2 = conn.prepareStatement(sqlCmd)) {
                    ps2.setString(1, address.getStreet());
                    ps2.setString(2, address.getPostalCode());
                    ps2.setString(3, address.getCity());
                    ps2.setInt(4, address.getId());
                    ps2.executeUpdate();
                    return true;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClientDB.class.getName()).log(Level.SEVERE, null, ex);
            dbc.registerError(ex);
            return false;
        }
    }

    @Override
    public boolean delete(Object dbConnection, Object object) {
        DatabaseConnection dbc = (DatabaseConnection)dbConnection;
        Connection conn = dbc.getConnection();
        Address address = (Address)object;
        //
        try {
            String sqlCmd;
            sqlCmd = "delete from address where addressId = ?";
            try (PreparedStatement ps = conn.prepareStatement(sqlCmd)) {
                ps.setInt(1, address.getId());
                ps.executeUpdate();
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClientDB.class.getName()).log(Level.SEVERE, null, ex);
            dbc.registerError(ex);
            return false;
        }
    }
}
