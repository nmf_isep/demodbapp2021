/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dal;

import app.domain.model.Address;
import app.domain.model.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nunocastro
 */
public class ClientDB implements IPersistable{

    @Override
    public boolean save(Object dbConnection, Object object) {

        DatabaseConnection dbc = (DatabaseConnection) dbConnection;
        Connection conn = dbc.getConnection();
        Client client = (Client) object;

        // verifica a existência de registo do cliente
        String sqlCmd;
        sqlCmd = "select * from client where clientId = ?";
        // cria objeto para executar a query
        try (PreparedStatement pscli = conn.prepareStatement(sqlCmd)) {
            pscli.setInt(1, client.getClientId());

            // executa a query
            try (ResultSet rscli = pscli.executeQuery()) {
                // se o cliente existir
                if (rscli.next()) {
                    sqlCmd = "update client set name = ?, vatnr = ? where clientId = ?";
                }               
                else {
                    sqlCmd = "insert into client(name, vatnr, clientId) values (?, ?, ?)";
                }
                // executa comando update ou insert
                try (PreparedStatement ps = conn.prepareStatement(sqlCmd)) {
                    ps.setString(1, client.getName());
                    ps.setString(2, client.getVatNr());
                    ps.setInt(3, client.getClientId());
                    ps.executeUpdate();
                }

                //Delete addresses.
                sqlCmd = "select * from clientaddress where clientId = ?";
                try (PreparedStatement ps = conn.prepareStatement(sqlCmd)) {
                    ps.setInt(1, client.getClientId());
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            boolean found = false;
                            for (int i = 0; i < client.getAddressList().size(); i++) {
                                 Address address = client.getAddressList().get(i);
                                 Integer addressId = rs.getInt("addressId");
                                 if (Objects.equals(addressId, address.getId())) {
                                     found = true;
                                     break;
                                 }
                            }
                            if (!found) {
                                sqlCmd = "delete from clientaddress where clientId = ? and addressId = ?";
                                try (PreparedStatement psdel = conn.prepareStatement(sqlCmd)) {
                                    psdel.setInt(1, client.getClientId());
                                    psdel.setInt(2, rs.getInt("addressId"));
                                    psdel.executeUpdate();
                                }
                            }
                        }
                    }
                }

                //Post new addresses.
                AddressDB adb;
                adb = new AddressDB();
                for (int i = 0; i < client.getAddressList().size(); i++) {
                    Address address = client.getAddressList().get(i);
                    if (!adb.save(dbc, address)) {
                        throw dbc.getLastError();
                    }
                    //Post association between clients ans addresses.
                    sqlCmd = "select * from clientaddress where clientId = ? and addressId = ?";
                    try (PreparedStatement pscliaddr = conn.prepareStatement(sqlCmd)) {
                        pscliaddr.setInt(1, client.getClientId());
                        pscliaddr.setInt(2, address.getId());
                        try (ResultSet rs = pscliaddr.executeQuery()) {
                            if (!rs.next()) {
                                sqlCmd = "insert into clientaddress(clientId, addressId) values (?, ?)";
                                try (PreparedStatement ps = conn.prepareStatement(sqlCmd)) {
                                    ps.setInt(1, client.getClientId());
                                    ps.setInt(2, address.getId());
                                    ps.executeUpdate();
                                }
                            }
                        }
                    }
                }
                //Save changes.
                return true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(ClientDB.class.getName()).log(Level.SEVERE, null, ex);
            dbc.registerError(ex);
            return false;
        }
    }
    
    @Override
    public boolean delete(Object dbConnection, Object object) {
        DatabaseConnection dbc = (DatabaseConnection)dbConnection;
        Connection conn = dbc.getConnection();
        Client client = (Client)object;
        //
        try {
            String sqlCmd;
            sqlCmd = "delete from clientaddress where clientId = ?";
            try (PreparedStatement ps = conn.prepareStatement(sqlCmd)) {
                ps.setInt(1, client.getClientId());
                ps.executeUpdate();
            }
            //
            sqlCmd = "delete from client where clientId = ?";
            try (PreparedStatement ps = conn.prepareStatement(sqlCmd)) {
                ps.setInt(1, client.getClientId());
                ps.executeUpdate();
            }
            //
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ClientDB.class.getName()).log(Level.SEVERE, null, ex);
            dbc.registerError(ex);
            return false;
        }
    }
}
