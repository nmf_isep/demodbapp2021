package app.domain.shared;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Constants {
    public static final String ROLE_ADMIN = "ADMINISTRATOR";

    public static final String PARAMS_FILENAME = "src/main/resources/config.properties";
    public static final String PARAMS_COMPANY_DESIGNATION = "Company.Designation";

    public static final String PARAMS_DATABASE_URL = "database.url";
    public static final String PARAMS_DATABASE_USERNAME = "database.username";
    public static final String PARAMS_DATABASE_PASSWORD = "database.password";

}
