/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.domain.model;

import app.dal.ConnectionFactory;

/**
 *
 * @author nunocastro
 */
public class Context {

    private static ConnectionFactory cf;
    
    private Context() {
        cf = new ConnectionFactory();
    }
    
    public static Context getInstance() {
        return ContextHolder.INSTANCE;
    }
    
    private static class ContextHolder {

        private static final Context INSTANCE = new Context();
    }

    public ConnectionFactory getConnectionFactory() {
        return cf;
    }
}
