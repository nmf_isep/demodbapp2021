/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.domain.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nunocastro
 */
public class Client {

    private Integer clientId;
    private String name;
    private String vatNr;
    private List<Address> addressList;

    public Client() {
        this.addressList = new ArrayList<>();
    }
    
    public Integer getClientId() {
        return clientId;
    }

    public void setId(Integer clientId) {
        this.clientId = clientId;
    }

    public String getName() {
        return name;
    }

    public String getVatNr() {
        return vatNr;
    }

    public void setVatNr(String vatNr) {
        this.vatNr = vatNr;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Address> getAddressList() {
        return addressList;
    }

    public void addAddress(Address address) {
        if (!this.addressList.contains(address)) {
            this.addressList.add(address);
        }
    }
}
